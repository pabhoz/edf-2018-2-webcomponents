class FancyTitle extends HTMLElement{

    constructor() {
        super();
        this._shadow = this.createShadowRoot();
        this._title = "";
        let template = `
            <style>
                .fancyTitle{
                    font-size: 300%;
                    font-weight: bolder;
                    font-family: Arial, Helvetica, sans-serif;
                }
            </style>
            <div class="fancyTitle">${this.title}</div>
        `;
        this.shadow.innerHTML = template;
    }

    get shadow() {
        return this._shadow;
    }

    set shadow(val) {
        this._shadow = val;
    }

    get title(){
        return `Im so fancy ${this._title}`;
    }

    set title(value){
        this._title = value;
    }

    static get observedAttributes() {
        return ['title'];
    }

    attributeChangedCallback(name, oldVal, newValue) {
        this[`update${name.charAt(0).toUpperCase() + name.slice(1)}`](oldVal,newValue);
        console.log(`vea, mi mamá manda a decir que ${name}, cambió de ${oldVal} a ${newValue}`);
    }

    updateTitle(oldVal,newValue){
        this.title = newValue;
        let el = this.shadow.querySelector(".fancyTitle").innerHTML = this.title;
    }

    connectedCallback() {
        
    }
}
window.customElements.define('fancy-title', FancyTitle);